[^ lesson home](../README.md)  |  [next episode >](./01_move_data.md)

----

# Getting started

## lesson objectives

- Have an understanding of how the HPC works (login node, compute node, job scheduler, filesystem)
- Ensure all necessary software is installed
- Login to the university HPC
- Mount the HPC filesystem on the local machine
- (optional) Set password-less login

## Overview

A typical HPC is organised as the following:

- A *login* or *head* node: this is the computer that the user connects to and submits jobs to be run.
- Several *compute* nodes: these are the computers that will actually do the hard work of running jobs.
- Storage that is shared across all the nodes.
- A *job scheduler* software: this is a program that manages all the jobs submitted by the users, puts them in a queue until there are compute nodes available to run the job.

Here is a schematic summary of the university HPC:

![University HPC](../images/uni_hpc_schematic.svg)

Here is a schematic summary of the small HPC at SLCU:

![SLCU HPC](../images/slcu_hpc_schematic.svg)


#### Do it Yourself

- Login to the University HPC using `ssh`
- Explore what files/folders are in your home directory

```bash
# connect to the hpc (replace <user> with your CRSid username)
ssh <user>@login.hpc.cam.ac.uk

# list files in home directory
ls -l
```

## HPC filesystem

### University HPC

There are two main storage locations of interest available on the HPC:

- `/home/$USER` is the user's home directory. It has a 40GB quota and is backed up. This should be used for example for local software and perhaps some very generic scripts.
- `/rds/user/$USER/hpc-work` is the user's working directory. It has a 1TB quota and is _NOT_ backed up (more space [can be purchased](https://www.hpc.cam.ac.uk/research-data-storage-services/price-list)).

When you login to the HPC you will notice there is a link (aka shortcut) to the `rds` directory. Try `ls -l` to see it.

<!--
[Note: there's also a shortcut to `/rcs/user/$USER`. This is access to "cold storage", which is the long-term slow-access storage provided by the university. Most likely you will not be using this unless you want to access/deposit archival data.]
-->

### SLCU HPC

On the SLCU HPC we do not have separate home and working directories (yet).

- The `/home` directory has 11TB of storage shared across all the users.
  - This means you should be mindful of the amount of storage you're using.
- This storage is _NOT_ backed-up, so make sure you transfer files out of there often (especially your scripts!).


## Mounting HPC filesystem

To work on scripts and quickly look at the output from your jobs, it's useful to mount the HPC storage to your local machine, so you can browse files and edit scripts with your favourite text editor.

### Linux/Mac

First, make sure you create a directory on our computer where you want the HPC filesystem to be mounted:

- Open a terminal
- Create a _mount_ directory for the hpc: `mkdir -p $HOME/mount/hpc_uni`

(the above only has to be done once)

Then, to mount the HPC filesystem run (replace `<user>` with your HPC username):

- `sshfs <user>@login.hpc.cam.ac.uk:/rds/user/<user>/hpc-work $HOME/mount/hpc_uni`
  - this has to be done every time you restart your machine

(**Note:** if you want to mount your HPC `/home`, then replace the relevant path above after `:`)

### Windows

Open _SFTP Drive_ and click the <kbd>New...</kbd> button on the right.
Then fill in the following information:

- _Drive Name_: hpc_uni (or any other name of your choice).
- _Drive Letter_: leave the default or pick a drive letter if you prefer.
- _Remote host_: login.hpc.cam.ac.uk
- _Username_: your CRSid
- _Password_: your raven password
- Under _Remote Folder_ choose _User's home folder_
- Press <kbd>OK</kbd>

From here on, when you want to connect/disconnect to the HPC filesystem you can press the <kbd>Start</kbd> / <kbd>Stop</kbd> button on the top menu.

<!--
- Open your windows explorer <img src=https://upload.wikimedia.org/wikipedia/en/thumb/0/04/File_Explorer_Icon.png/64px-File_Explorer_Icon.png width=20 height=20 />
- On the left menu <kbd>right-click</kbd> on _This PC_ (Windows 10) or _Computer_ (Windows 7) and select _Map network drive..._
- Select a _Drive_ letter of your choice
- Type the following in _Folder:_ `\\sshfs\<user>@login.hpc.cam.ac.uk\rds\hpc-work` (replace `<user>` with your HPC username)
-->

#### Do it Yourself

Prepare a working directory for your projects on the HPC:

- From your HPC terminal `cd` to the `hpc-work` directory
- Create a new directory within it called "stuff"
- Using the file browser on your computer, go the the HPC mounted drive and change the directory name to "projects"
- Go back to the HPC terminal and check that the directory name changed

```bash
# cd to hpc-work directory
cd ~/rds/hpc-work

# Create directory called stuff (note that variable $USER can be replaced by your username)
mkdir stuff

# Change directory name from the terminal (the exercise wants us to change it from the file browser)
mv stuff projects
```

## Set password-less login

To avoid having to type your password every time you login to the HPC, follow these instructions:

1. Open a new terminal (do not login to the HPC)
2. Check whether you already have a _SSH key_: `ls -al ~/.ssh/id_*.pub`. If you do, skip to step 4.
3. Run `ssh-keygen -t rsa` to create a new _SSH key_ 
   - When asked "Enter passphrase" it is recommended that you create one rather than leaving it empty. Then run `eval 'ssh-agent'` followed by `ssh-add` (you will be asked for the passphrase). This ensures the file containing your _SSH key_ is encrypted. 
4. Copy the _key_ to the hpc (you will be asked for your HPC password):
   - `ssh <user>@login.hpc.cam.ac.uk mkdir -p .ssh` 
   - Then `cat .ssh/id_rsa.pub | ssh <user>@login.hpc.cam.ac.uk 'cat >> .ssh/authorized_keys'`

You can now login to the HPC without having to type a password!

## Additional resources

- [UIS Documentation on filesystem](https://docs.hpc.cam.ac.uk/hpc/user-guide/io_management.html)
- [Price list for HPC storage](https://www.hpc.cam.ac.uk/research-data-storage-services/price-list)

----

[^ lesson home](../README.md)  |  [next episode >](./01_move_data.md)
