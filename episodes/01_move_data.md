[^ lesson home](../README.md)  |  [< previous episode](./00_getting_started.md)  |  [next episode >](./02_slurm_basics.md)

----

# Moving data to remote server

## Lesson objectives

- Move data between local and remote server using command line tools

## Prepare project data

Before starting any project it's a good idea to start with a "tidy" directory structure.
We will work with some sequence data (only as an example), so let's download it to a "data" directory.

Open a new terminal and run the following commands to setup your project directory and get the data:

```bash
# Create project directory on Desktop
mkdir -p ~/Desktop/hpc_workshop/data
cd ~/Desktop/hpc_workshop/data  # move into that directory

# download data
# Linux
wget https://hpc-carpentry.github.io/hpc-shell/files/bash-lesson.tar.gz
# MacOS
curl -O https://hpc-carpentry.github.io/hpc-shell/files/bash-lesson.tar.gz
```

#### Where are my files?

Often it can be confusing how to access the files you see on your file browser from the terminal.
Here's a few useful locations. (replace <user> with your username in the examples below)

**MacOS**

- Home is located at `/Users/<user>`; the symbol `~` is a shortcut for your home directory.
- Desktop: `cd ~/Desktop`
- Documents: `cd ~/Documents`

**Windows**

The windows drives (e.g. the `C:` drive) are in `/mnt/` (Ubuntu subsystem) or `/drives/` (MobaXterm).

Assuming you're using the _MobaXterm_:

- "C:" drive: `cd /drives/c/`
- Documents: `cd /drives/c/Users/<user>/Documents`
- Desktop: `cd /drives/c/Users/<user>/Desktop`

MobaXterm conveniently provides symbolic links (aka shortcuts) to _Documents_ and _Desktop_, so for those two locations you can actually do:

- Desktop: `cd ~/Desktop`
- Documents: `cd ~/MyDocuments`


## How to move files

There are several options to move data between your local computer and a remote server.

- With the filesystem mounted [as we did earlier](./00_getting_started.md) you can use your file browser and "copy-paste" files between folders.
  This solution is easy but may not always be very stable (especially for larger files).
- Use tools dedicated for this purpose. We will cover some possibilities in this section, in order of ease of use.


### Filezilla (GUI)

This program has a graphical interface, for those that prefer it and its use is relatively intuitive.

To connect to the remote server fill the following information on the top panel:

- Host: login.hpc.cam.ac.uk
- Username: your HPC username
- Password: your HPC password
- Port: 22

Click "Quickconnect" and your files should appear in a panel on right side.
You can then drag-and-drop files between the left side panel (your local filesystem) and the right side panel (the HPC server filesystem).


### `scp` (command line)

This is a common alternative to _rsync_, but always transfers all files (regardless of whether they have changed or not):

```bash
# copy files from the local computer to the HPC
scp -r path/to/source_folder <user>@login.hpc.cam.ac.uk:path/to/target_folder

# copy files from the HPC to a local directory
scp -r <user>@login.hpc.cam.ac.uk:path/to/source_folder path/to/target_folder
```

- the option `-r` ensures that all sub-directories are copied (instead of just files)


### `rsync` (command line)

This program is advised because it has options to synchronise only those files that have changed between your _source_ and _target_ directories.

The most common usage is:

```bash
# copy files from the local computer to the HPC
rsync -auvh --progress path/to/source_folder <user>@login.hpc.cam.ac.uk:path/to/target_folder

# copy files from the HPC to a local directory
rsync -auvh --progress <user>@login.hpc.cam.ac.uk:path/to/source_folder path/to/target_folder
```

- the options `-au` ensure that only files that have changed _and_ are newer on the source folder are transferred
- the options `-vh` give detailed information about the transfer and human-readable file sizes
- the option `--progress` shows the progress of each file being transferred

**Important note:**

When you specify the *source* directory as `path/to/source_folder/` (with `/` at the end) or `path/to/source_folder` (without `/` at the end), `rsync` will do different things:

  - `path/to/source_folder/` will copy the files *within* `source_folder` but not the folder itself
  - `path/to/source_folder` will copy the actual `source_folder` as well as all the files within it

**tip:** to check what files `rsync` would transfer but not actually transfer them, add the `--dry-run` option. This is useful to check that you've specified the right source and target directories and options.


## Do it Yourself

Using the terminal transfer your project directory to the HPC, using either `rsync`, `scp` or _Filezilla_.
Make sure you transfer the files to your projects directory `rds/hpc-work/projects/`

Note, this should be done _from your local machine_ (not from the HPC):

```bash
# using rsync
# this only transfers files that have changed in relation to the target
rsync -avhu --progress hpc_workshop <user>@login.hpc.cam.ac.uk:rds/hpc-work/projects/

# using scp
# copies everything, does not check if files are older/newer in the target
scp -r hpc_workshop <user>@login.hpc.cam.ac.uk:rds/hpc-work/projects/
```

Once you've transferred the files, decompress the data folder. Note, this is now done _logged in on the HPC_:

```bash
# go the the projects directory
cd ~/rds/hpc-work/projects/hpc_workshop/data

# decompress the files
tar -xzf bash-lesson.tar.gz
```

## Additional resources

- [HPC-Carpentry lesson on file transfer](https://hpc-carpentry.github.io/hpc-intro/15-transferring-files/index.html)
- [UIS Documentation on file transfer](https://docs.hpc.cam.ac.uk/hpc/user-guide/transfer.html)

----

[^ lesson home](../README.md)  |  [< previous episode](./00_getting_started.md)  |  [next episode >](./02_slurm_basics.md)
